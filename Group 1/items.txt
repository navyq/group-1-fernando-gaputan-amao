330 Ohm Resistor	7	1.25
1K Ohm Resistor	10	1.25
10K Ohm Resistor	10	1.25
22K Ohm Resistor	10	1.25
10K Rotary Potentiometer	20	10
Light Dependent Resistor (LDR)	10	2.00
Green LED 5mm	20	3.00
Red LED 5mm	20	3.00
Yellow LED 5mm	20	3.00
Push Button Switch	15	15.00
10uF Electrolytic Capacitor	10	3.00
100uf Electrolytic Capacitor	10	10.00
0.1uF Ceramic Capacitor	10	3.00
Transistor BC546 NPN	10	13.00
Diode (1N4007)	20	2.00
Breadboard Prototype Wires	10	15.00
5V Regulator LM7805	8	12.00
Battery Snap	7	12.00
Breadboard 400 hole	2	60.00
